export const server = {
    host: process.env.HOST || '0.0.0.0',
    port: process.env.PORT || 8080,
    storageDir: '/tmp/storage/data',
    tmpUploadDir: '/tmp/storage/tmp'
}

export const jwt = {
    secret: process.env.JWT_SECRET || 'some_secretData',
    accessTokenExpiration: process.env.ACCESS_TOKEN_EXPIRATION || 86400, //1d
};

//Name Servers that we will interract
export const ns = {
    serverList: [
        process.env.NS_IP || '10.1.1.113:8888' 
    ],
    timeout: 5000, //how much time we will wait to NS server answer
    tokenSavePath: `/tmp/storage/ns`//folder which will contain alredy obtained tokens from NS
    // authToken:'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjAifQ.IwzM8EBDAcob6KCS23vKaTPBmAuEu4zr1-gia0eWVwM',
}
