export const server = {
    host: process.env.HOST || '0.0.0.0',
    port: process.env.PORT || 8080,
    storageDir:'/tmp/storage',
    tmpUploadDir: '/tmp/storage/tmp'
}

export const jwt = {
    secret: process.env.JWT_SECRET || 'some_secretData',
    accessTokenExpiration: process.env.ACCESS_TOKEN_EXPIRATION || 86400, //1d
};

//Name Servers that we will interract
export const ns = {
    serverList: [
        '10.1.1.113:8080'
    ],
    timeout: 5000, //how much time we will wait to NS server answer
    tokenSavePath: `${__dirname}/ns`//folder which will contain alredy obtained tokens from NS
}