import fs from 'fs'
import path from 'path'
import axios from 'axios'
import { ns as nsCfg } from '../config'
import { pingNS } from './ping'
import Logger from '../logger';
const log = Logger(__filename);

let CUR_NS = {
    address: null,
    token: null,
}
let IS_FIRST_LAUNCH = true
const chooseNSserver = async () => {
    return new Promise(async (resolve, reject) => {
        let existingTokens = getExistingTokens(nsCfg.tokenSavePath)

        for (let nsUrl of nsCfg.serverList) {
            try {
                let tokenKey = `${process.env.HOST || '0.0.0.0'}_${nsUrl}`//key(file name) in existingTokens. 
                let tokenToReq;
                //if this is our first lauch and there are no NS server auth tokens
                if (!existingTokens[tokenKey]) {
                    log.info(`Attempt register in ${nsUrl}`)
                    //register request
                    let nsResponse = await axios({
                        method: 'post',
                        url: `http://${nsUrl}/reg`,
                        timeout: nsCfg.timeout,
                    });
                    if (nsResponse.status !== 200) {
                        log.error(`Can't register in ${nsUrl}. Reason: ${JSON.stringify(nsResponse.data)}`)
                        continue
                    }
                    //save token to file
                    let nsToken = nsResponse.data.token
                    let nsTokenSavePath = path.join(nsCfg.tokenSavePath, tokenKey)
                    fs.writeFileSync(nsTokenSavePath, nsToken)
                    tokenToReq = nsToken
                    log.info(`Register in ${nsUrl} sucessfull. Token file saved to ${nsTokenSavePath} and will be used to connect without registration`)
                }
                //update global var 
                CUR_NS = {
                    address: nsUrl,
                    token: existingTokens[tokenKey] || tokenToReq
                }
                let nsResponse = await axios({
                    method: 'post',
                    url: `http://${nsUrl}/auth/storage`,
                    headers: { 'Authorization': existingTokens[tokenKey] || tokenToReq },
                    timeout: nsCfg.timeout,
                });
                if (nsResponse.status !== 200) {
                    log.error(`Can't register in ${nsUrl}. Reason: see below`)
                    console.log('debug about register err', nsResponse.data)
                    continue
                }
                log.info(`Auth sucessfull. Connection with ${nsUrl} established.`)

                if (IS_FIRST_LAUNCH) {
                    pingNS(nsResponse.data.uid)
                }
                IS_FIRST_LAUNCH = false
                return resolve()
            } catch (error) {
                log.error(`Can't reach ${nsUrl}. Reason: ${error.name} ${error.message}`)
            }
        }
        return reject({ name: 'ALL_NS_UNAVAIL', message: 'All name servers unavailable' })
    })
}

const getExistingTokens = (folderPath) => {
    let existingTokens = {}
    let filesList = fs.readdirSync(folderPath)

    for (let file of filesList) {
        let fullPathToFile = path.join(folderPath, file)
        let fileContent = fs.readFileSync(fullPathToFile, { encoding: 'utf-8' })
        existingTokens[file] = fileContent
    }
    return existingTokens
}
const getCurNS = () => {
    return CUR_NS
}

export { chooseNSserver, getCurNS }