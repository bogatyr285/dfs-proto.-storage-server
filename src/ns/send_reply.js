import axios from 'axios'
import { getCurNS } from './index'

import Logger from '../logger';
const log = Logger(__filename);

const sendReplyToNS = async ({ reqId, paramUrl, confirmId, isRepl }) => {
    const curNSConf = getCurNS()
    log.info(`#${reqId}. Send confirmation request to NS ${curNSConf.address}`)
    let url = `http://${curNSConf.address}${paramUrl}`//paramUrl begin with /
    let i = 0;
    //TODO here need a deleting file if we cant confirm on NS, but not today
    while (true) {
        i++;
        if (i > 10) {//reset counter if we wait many time. 
            i = 0
        }
        try {
            let response = await axios({
                method: 'post',
                url,
                headers: {
                    'Authorization': curNSConf.token,
                    'id': confirmId,
                    'isrepl': isRepl ? true : false
                },
            })
            log.info(`#${reqId}. Confirmation request sucessfull`)
            return;
        } catch (error) {
            log.error(`#${reqId}. Iteration:${i} URL:${url} Confirmation id:${confirmId} request error: ${JSON.stringify(error.status || error.message)}`)
            await sleep(Math.pow(2, i) * 1000);
            continue
        }
    }
}
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
export { sendReplyToNS }