import fs from 'fs'
import path from 'path'
import axios from 'axios'
import { ns as nsCfg } from '../config'
import { getCurNS, chooseNSserver } from './connect'
import Logger from '../logger';
const log = Logger(__filename);

const pingNS = async (uid) => {
    return new Promise(async (resolve, reject) => {
        const curNSConf = getCurNS()
        while (true) {
            log.debug(`Regular ping NS server:${curNSConf.address}`)
            try {
                let nsResponse = await axios({
                    method: 'post',
                    url: `http://${curNSConf.address}/ping`,
                    timeout: nsCfg.timeout,
                    headers: {
                        'uid': uid
                    },
                });

                log.debug(`Ping NS answered ${nsResponse.status}`)
            } catch (error) {
                if (error.response && error.response.status === 400) {
                    log.debug(`Ping NS answered with 'need reauth'`)
                    await chooseNSserver()
                }
                log.warn(`Regular ping to NS ${curNSConf.address} failed.`)
                continue
            } finally {
                await sleep(5000)
            }
        }
    })
}
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export { pingNS }