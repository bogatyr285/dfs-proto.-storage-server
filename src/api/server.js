
import Koa from 'koa';
import router from './routes'
import KoaBody from 'koa-body'
import body from 'koa-better-body'
import cors from '@koa/cors';
import passport from './auth';
import { server as servCfg } from '../config'
import { setRequestId, responseTime, errorsHandler, } from './middlewares'

import Logger from '../logger';
const log = Logger(__filename);

import formidable from 'formidable'

const setup = () => {

    let form = new formidable.IncomingForm()
    form.keepExtensions = true
    form.uploadDir = servCfg.tmpUploadDir
    form.maxFileSize = 10000 * 1024 * 1024; //10 GB

    const server = new Koa();
    return server
        .use(errorsHandler)
        .use(cors())
        .use(passport.initialize())
        .use(passport.session())
        .use(body({
            multipart: true,
            IncomingForm: form
        }))
        .use(setRequestId)
        .use(responseTime)
        //routers
        .use(router.routes())
        .use(router.allowedMethods())
        .listen({
            host: servCfg.host,
            port: servCfg.port,
            exclusive: true
        }, () => { log.info(`Server running. http://${servCfg.host}:${servCfg.port}  Environment: ${process.env.NODE_ENV} `); })
}

export default setup
