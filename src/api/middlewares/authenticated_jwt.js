import jwt from 'jsonwebtoken'
import { jwt as jwtCfg } from '../../config'

import Logger from '../../logger'
const log = Logger(__filename);

const authenticated_jwt = async (ctx, next) => {
    const { authorization } = ctx.headers;
    //   log.debug(`#${ctx.state.reqId}. Checking token: ${authorization}`);
    if (!authorization) {
        ctx.throw(400, `Didn't provide access token`);
    }

    const tokenBody = authorization.split(' ')[1]; //0 - header(Access); 1 - token body

    try {
        let payload = await getPayload(tokenBody);
    } catch (error) {
        ctx.throw(401, { name: error.name, message: error.message })
    }

    await next()
}

async function getPayload(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, jwtCfg.secret, (err, decoded) => {
            if (err) return reject(err)
            return resolve(decoded)
        })
    })
}
export default authenticated_jwt