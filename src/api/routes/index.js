import Router from 'koa-router';
const router = new Router({ prefix: '/api' });

import filesRoutes from './files'
import { authenticated_jwt } from '../middlewares';
import  statusHandler  from './status';

filesRoutes(router)

router.get('/status', /* authenticated_jwt, */ statusHandler);

export default router;