import Logger from '../../logger';
const log = Logger(__filename);

const statusHandler = async ctx => {
    ctx.body = {
        status: 'ok',   
    }
};

export default statusHandler