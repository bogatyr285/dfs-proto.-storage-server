import { server as servCfg } from '../../../config'
import path from 'path'
import fs from 'fs'
import Logger from '../../../logger';
const log = Logger(__filename);

const getFileListHandler = async ctx => {
    if (typeof ctx.request.fields==='undefined') {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Params not found" })
    }
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(ctx.request.fields)}`);

    const { path: requestedPathParam } = ctx.request.fields
    if (!requestedPathParam) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Path unspecified" })
    }
    let isAbsolutePath = path.isAbsolute(requestedPathParam)//check if provided path not begin from '/'
    if (!isAbsolutePath) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Incorrect unspecified" })
    }

    const requestedPath = path.join(servCfg.storageDir, requestedPathParam);

    if (requestedPath.indexOf(servCfg.storageDir) === -1) { // if user try to escape from storage dir
        ctx.throw(400, { name: 'INPUT_ERR', message: 'Incorrect path' })
    }

    try {
        const fileList = walkSync(requestedPath)
        log.info(`#${ctx.state.reqId}. ok`);
        return ctx.body = {
            status: 'ok',
            response: {
                path: requestedPathParam,
                files: fileList
            }
        }
    } catch (error) {
        if (error.message.indexOf('no such file or directory') !== -1) {
            ctx.throw(416, { name: 'FILE_NOT_FOUND', message: 'No such directory' })//"range not satisfiable"
        }
        ctx.throw(500, error)
    }
};

// List all files in a directory
function walkSync(dir, _filelist) {
    let files = fs.readdirSync(dir);
    let filelist = _filelist || [];
    files.forEach((file) => {
        let fullPathToFile = path.join(dir, file)
        let fileStat = fs.statSync(fullPathToFile)

        let fileInfo = {
            name: file,
            isDirectory: fileStat.isDirectory(),
            size: fileStat.size,
            path: fullPathToFile.split(servCfg.storageDir)[1] //delete our full path to file and make it relatively to user
        }
        filelist.push(fileInfo);
    });
    return filelist;
};

export default getFileListHandler