import path from 'path'
import fs from 'fs'
import { sendReplyToNS } from '../../../ns'
import { server as servCfg } from '../../../config'

import Logger from '../../../logger';
import axios from 'axios';
const log = Logger(__filename);

const synchronizeFileHandler = async (ctx, next) => {
    if (typeof ctx.request.fields === 'undefined') {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Params not found" })
    }
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(ctx.request.fields)}`);
    const { path: requestedUploadFolder, id: confirmId, isrepl: isRepl } = ctx.request.fields;//url - from what server we will upload
    const url = ctx.request.fields.url || ''
    if (!requestedUploadFolder) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Path unspecified" })
    }
    if (!confirmId) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Request file confirmation id unspecified" })
    }

    //TODO some problem with URL:port regex
    /* let urlRegex = new RegExp(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi)
    console.log('url',urlRegex.test(url))
    if (!urlRegex.test(url)) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "URL unspecified" })
    } */
    if (!url) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "URL unspecified" })
    }

    let folderAldreadyExist = fs.existsSync(path.join(servCfg.storageDir, requestedUploadFolder))
    if (!folderAldreadyExist) {
        ctx.throw(409, { name: 'FOLDER_UNEXIST', message: 'Requested folder non exist. You should create it first' })
    }

    let fullFilePath;
    try {
        let requestToFile = await axios({
            method: 'GET',
            url,
            responseType: 'stream'
        })

        if (typeof requestToFile.headers['content-disposition'] === 'undefined') {
            return ctx.throw(503, { name: 'REPLY_ERR', message: `Requested server answered without 'content-disposition' field` })
        }
        let filename = requestToFile.headers['content-disposition'].match(/"(.*?)"/)[1]
        fullFilePath = path.join(servCfg.storageDir, requestedUploadFolder, filename)

        ///!!! now we rewrite existing file. NS cant make a mistake
        /*  let fileAlreadyExist = fs.existsSync(fullFilePath)
         if (fileAlreadyExist) {
             ctx.throw(409, { name: 'FILE_EXIST', message: 'File already exist' })
         } */

        requestToFile.data.pipe(fs.createWriteStream(fullFilePath))
    } catch (error) {
        const statusCode = error.statusCode || error.status;
        if (statusCode) {
            ctx.throw(statusCode, error)
        }
        // if err thrown by broken code
        if (typeof error.message !== 'undefined') {
            ctx.throw(500, { name: 'INTERNAL_ERR', message: `${error.name} ${error.message}` })
        }

        let response = ''
        error.response.data.on('data', (chunk) => {
            response += chunk.toString()
        }).on('end', () => {
            let servErrMsg = JSON.parse(response).error
            ctx.throw(503, { name: 'SYNC_FAILED', message: `Requested server return error`, reply: servErrMsg })
        })
    }
    //!!! call function without await coz we want to send answer to user and after make a confirm processing
    sendReplyToNS({ reqId: ctx.state.reqId, confirmId, paramUrl: '/storage/confirm', isRepl })

    log.info(`#${ctx.state.reqId}. ok`);
    return ctx.body = {
        status: 'ok',
        response: {
            file: fullFilePath.split(servCfg.storageDir)[1], //delete our full path to file and make it relatively to user,
            synchronizedWith: url
        }
    };
};

export default synchronizeFileHandler