import getFileListHandler from './get_list'
import getFileUploadHandler from './upload'
import downloadFileHandler from './download'
import deleteHandler from './remove'
import createFolder from './create_folder'
import isFileExist from './is_exist'
import synchronize from './synchronize'
import rename from './rename'
import { authenticated_jwt } from '../../middlewares';


//'/home/bogatyr/work/sandbox/DS.SS/tmp', : /* servCfg.tmpUploadDir */ // directory where files will be uploaded
export default (router) => {
    //http://localhost:8080/api/files/getlist?path=/

        /**
     * @api {get} /api/file/ GetFileList
     * @apiDescription Get list of files in specific directory
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName GetFileList
     * @apiGroup Files
     *
     * @apiParam {String} path Ful path to the directory what you want to look in.
     *
     * @apiExample Example usage:
     * curl -X GET http://localhost:8000/api/files/getlist
     *
     * @apiSuccess {String}     status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}     response
     * @apiSuccess {String}     response.path     Path what contains list of files
     * @apiSuccess {Array}      response.files    Array of files in directory
     * @apiSuccess {String}     response.files.name             Name of file
     * @apiSuccess {Boolean}    response.files.isDirectory      True if file is directory, false otherwise
     * @apiSuccess {Int}        response.files.size             Size of the file
     * @apiSuccess {Array}      response.files.path             Full name of the file
     *
     * @apiSuccessExample {json} Successful-Request:
     *      {
     *          "status": "ok",
     *          "response": {
     *              "path": "/test123",
     *              "files": [
     *                  {
     *                      "name": "1.txt",
     *                      "isDirectory": false,
     *                      "size": 20,
     *                      "path": "/test123/1.txt"
     *                  }
     *              ]
     *          }
     *      }
     *
     * @apiError (Error 400)  {String} status 'ok' or 'error'
     * @apiError (Error 400)  {Object} response
     * @apiError (Error 400)  {String} response.name name of the error
     * @apiError (Error 400)  {String} response.message description of the error

     * @apiError (Error 416)  {String} status 'ok' or 'error'
     * @apiError (Error 416)  {Object} response
     * @apiError (Error 416)  {String} response.name Name of the error 'FILE_NOT_FOUND'
     * @apiError (Error 416)  {String} response.message description of the error
     * @apiErrorExample {json} Error-Response:
     *      {
     *          "status": "error",
     *          "error": {
     *              "name": "FILE_NOT_FOUND",
     *              "message": "No such directory"
     *          }
     *      }
     *
     */
    router.post('/files/getlist', /* authenticated_jwt, */ getFileListHandler)

        /**
     * @api {get} /api/file/ DownloadFile
     * @apiDescription DownloadFile from server
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName DownloadFile
     * @apiGroup Files
     *
     * @apiExample Example usage:
     * curl -X GET http://localhost:8000/api/files/download?path=/1.txt
     *
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccessExample {json} Successful-Request:
     *     data....
     * @apiError (Error 400) {String}   status            Request status. 'ok' or 'error'
     * @apiError (Error 400) {Object}   response          Response
     * @apiError (Error 400) {String}   response.name Name of the error is 'INPUT_ERR'
     * @apiError (Error 416) {String}   response.name Name of the error is 'FILE_NOT_FOUND'
     * @apiErrorExample {json} Error-Response:
     *       {
     *           "status": "error",
     *           "error": {
     *               "name": "FILE_NOT_FOUND",
     *               "message": "Requested file not found"
     *           }
     *       }
     *
     */
    router.get('/files/download', downloadFileHandler)

     /**
     * @api {post} /api/files/ DeleteFile/Folder
     * @apiDescription Delete file or folder from file server
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName DeleteFile/Folder
     * @apiGroup Files
     *
     * @apiExample Example usage:
     * curl -X POST   http://localhost:8080/api/files/remove
     *
     * @apiParam {String} path Full name of file/folder what you want to delete
     * @apiParam {Int} id confirmation id for NS server
     *
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response          Response
     * @apiSuccess {Object}   response.conversation  Info about created conv
     *
     *
     * @apiSuccessExample {json} Successful-Request:
     *      {
     *          "status": "ok",
     *          "response": {
     *              "removed": "/asdf"
     *          }
     *      }
     *
     * @apiError (Error 400) {String}   status            Request status. 'ok' or 'error'
     * @apiError (Error 400) {Object}   response          Response
     * @apiError (Error 400) {String}   response.name Name of the error is 'INPUT_ERR'
     * @apiError (Error 416) {String}   response.name Name of the error is 'FILE_NOT_FOUND'
     *
     * @apiErrorExample {json} Error-Response:
     *      {
     *          "status": "error",
     *          "error": {
     *              "name": "INPUT_ERR",
     *              "message": "Path unspecified"
     *          }
     *      }
     *
     */
    router.post('/files/remove', deleteHandler)

    /**
     * @api {put} /api/files/ CreateFolder
     * @apiDescription create a folder with specific name (default name is 'new') at specific path
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName CreateFolder
     * @apiGroup Files
     *
     * @apiExample Example usage:
     * curl -X PUT   http://localhost:8080/api/files/createfolder
     *
     * @apiParam {String} name name of folder what you want to create
     * @apiParam {String} path path where you want to create new folder, starts from root
     * @apiParam {Int} id confirmation id for NS server
     *
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response          Response
     * @apiSuccess {String}   response.created    Full name of new created folder
     *
     * @apiError (Error 400) {String}   status          Request status. 'ok' or 'error'
     * @apiError (Error 400) {Object}   error           Response
     * @apiError (Error 400) {String}   error.name      Name of the error 'INPUT_ERR',
     * @apiError (Error 400) {String}   error.message   Error description
     *
     * @apiError (Error 400) {Object}   error           Response
     * @apiError (Error 416) {String}   error.name      Name of the error 'DIR_EXIST'
     *
     * @apiSuccessExample {json} UserFound:
     *      {
     *          "status": "ok",
     *          "response": {
     *              "created": "/asdf1"
     *          }
     *      }
     *
     * @apiErrorExample {json} DIR_EXIST :
     *      {
     *           "status": "error",
     *           "error": {
     *               "name": "DIR_EXIST",
     *               "message": "Directory already exist"
     *           }
     *      }
     *
     */
    router.post('/files/createfolder', createFolder)

        /**
     * @api {get} /api/file/ isFileExist
     * @apiDescription Check existing of the file
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName isFileExist
     * @apiGroup Files
     *
     * @apiParam {String} path Ful name of the file what you whant to check
     * @apiParam {String} size Size of the file what you check *
     * @apiExample Example usage:
     * curl -X GET http://localhost:8000/api/files/isexist
     *
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response
     * @apiSuccess {String}   response.message  ---------------
     *
     * @apiSuccessExample {json} Successful-Request:
     *     HTTP/1.1 200 OK
     *
     * @apiError (Error 400) BadRequest Invalid conversation id
     * @apiErrorExample {json} Error-Response:
     *      {
     *          "status": "error",
     *          "error": {
     *              "name": "INPUT_ERR",
     *              "message": "Params not found"
     *          }
     *      }
     *
     */
    router.post('/files/isexist', isFileExist)

        /**
     * @api {get} /api/file/ Synchronize
     * @apiDescription Download file from 'url' and put it to provided path
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName Synchronize
     * @apiGroup Files
     *
     * @apiParam {String} path Ful name of the file what you whant to synchronize
     * @apiParam {String} url file storage source url
     * @apiParam {Int} id
     *
     * @apiExample Example usage:
     * curl -X GET http://localhost:8000/api/files/ynchronize
     *
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response
     * @apiSuccess {String}   response.file     path where server put file
     * @apiSuccess {String}   response.synchronizedWith     url of source file
     *
     * @apiSuccessExample {json} Successful-Request:
     *      {
     *          "status": "ok",
     *          "response": {
     *              "file": "/home/bogatyr/work/sandbox/DS.SS/storage/storage/test123/CIA Lab 1_2 Assignment_ Booting (1).pdf",
     *              "synchronizedWith": "http://10.1.1.146:8080/api/files/download?path=/CIA Lab 1_2 Assignment_ Booting (1).pdf"
     *          }
     *      }
     *
     * @apiError (Error 400)  {String} status 'ok' or 'error'
     * @apiError (Error 400)  {Object} response
     * @apiError (Error 400)  {String} response.name name of the error 'INPUT_ERR', 'FOLDER_UNEXIST'
     * @apiError (Error 400)  {String} response.message description of the error
     * @apiErrorExample {json} Error-Response:
     *      {
     *          "status": "error",
     *          "error": {
     *              "name": "INPUT_ERR",
     *              "message": "Request file confirmation id unspecified"
     *          }
     *      }
     *
     */
    router.post('/files/synchronize', synchronize)

        /**
     * @api {post} /api/files/ UploadFile
     * @apiDescription Upload file to the server
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName UploadFile
     * @apiGroup Files
     *
     * @apiExample Example usage:
     * curl -X POST   http://localhost:8080/api/files/upload
     *
     * @apiParam {String} data File what you want to upload
     * @apiParam {String} path Full name of file where you want upload it and how to name it.
     * @apiParam {Int} id confirmation id for NS server
     *
* @apiSuccess {String}   status            Request status. 'ok' or 'error'
* @apiSuccess {Object}   response          Response
* @apiSuccess {Object}   response.conversation  Info about created conv
*
*
* @apiSuccessExample {json} Successful-Request:
*      {
*          "status": "ok",
*          "response": {
*              "removed": "/asdf"
*          }
*      }
*
* @apiError (Error 400) {String}   status            Request status. 'ok' or 'error'
* @apiError (Error 400) {Object}   response          Response
* @apiError (Error 400) {String}   response.name Name of the error is 'INPUT_ERR'
*
     * @apiErrorExample {json} Error-Response:
     *       {
     *           "status": "error",
     *           "error": {
     *               "name": "INPUT_ERR",
     *               "message": "Params not found"
     *           }
     *       }
     *
     */
    router.post('/files/upload', getFileUploadHandler)

    router.post('/files/rename', rename)

}

//curl -X POST  http://localhost:8080/api/files/upload?path=/ -H "Content-Type: multipart/form-data" -F "data=@storage/1.txt"
