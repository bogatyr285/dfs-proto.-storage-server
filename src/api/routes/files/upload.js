import path, { resolve } from 'path'
import fs from 'fs'
import moveFile from 'move-file'
import { sendReplyToNS } from '../../../ns'
import { server as servCfg } from '../../../config'

import Logger from '../../../logger';
const log = Logger(__filename);

const uploadFileHandler = async (ctx, next) => {
    if (typeof ctx.request.fields==='undefined') {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Params not found" })
    }
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(ctx.request.fields)}`);

    const requestedUploadFolder = ctx.request.fields.path || '';
    const confirmId = ctx.request.fields.id || '';
    if (!requestedUploadFolder) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Path unspecified" })
    }
    if (!confirmId) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Request file confirmation id unspecified" })
    }
    let isAbsolutePath = path.isAbsolute(requestedUploadFolder)//check if provided path not begin from '/'
    if (!isAbsolutePath) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Incorrect path" })
    }
    if (typeof ctx.request.files === 'undefined') { //TODO checking
        ctx.throw(400, { name: 'INPUT_ERR', message: "No file in attachemets" })
    }

    let attachedFile = ctx.request.files[0]
    //   let attachedFile = ctx.request.files.data with old body
    let newFilePath = path.join(servCfg.storageDir, requestedUploadFolder)
    let newFileFullPath = path.join(newFilePath, attachedFile.name)

    if (newFileFullPath.indexOf(servCfg.storageDir) === -1) { // if user try to escape from storage dir
        ctx.throw(400, { name: 'INPUT_ERR', message: 'Incorrect path' })
    }
    let fileAldreadyExist = fs.existsSync(newFileFullPath)
    if (fileAldreadyExist) {
        ctx.throw(409, { name: 'FILE_EXIST', message: 'File with that name already exist' })
    }

    //!!! call function without await coz we want to send answer to user and after make a confirm processing
    sendReplyToNS({ reqId: ctx.state.reqId, confirmId, paramUrl: '/storage/confirm' })

    await moveFile(attachedFile.path, newFileFullPath);

    log.info(`#${ctx.state.reqId}. ok`);
    return ctx.body = {
        status: 'ok',
        response: {
            file: {
                path: newFileFullPath.split(servCfg.storageDir)[1], //delete our full path to file and make it relatively to user,
                name: attachedFile.name,
                size: attachedFile.size
            }
        }
    };
};
export default uploadFileHandler