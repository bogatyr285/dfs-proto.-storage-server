import path from 'path'
import fs from 'fs'
import { server as servCfg } from '../../../config'
import { sendReplyToNS } from '../../../ns'
import Logger from '../../../logger';
const log = Logger(__filename);

const renameHandler = async (ctx, next) => {
    if (typeof ctx.request.fields==='undefined') {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Params not found" })
    }
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(ctx.request.fields)}`);
    const { path: requestedPath, name: requestedNewName } = ctx.request.fields;
    const confirmId = ctx.request.fields.id || '';
    if (!requestedPath) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Path unspecified" })
    }
    if (!confirmId) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Request file confirmation id unspecified" })
    }
    let isAbsolutePath = path.isAbsolute(requestedPath)//check if provided path not begin from '/'

    if (!isAbsolutePath) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Path unspecified" })
    }
    let fullPathToFile = path.join(servCfg.storageDir, requestedPath)
    let newFilePath = path.join(servCfg.storageDir, requestedPath, '..', requestedNewName)
    try {
        let fileExist = fs.existsSync(fullPathToFile)
        if (!fileExist) {
            ctx.throw(416, { name: 'FILE_NOT_FOUND', message: 'No such directory' })//"range not satisfiable"
        }
        let fileSameNameExist = fs.existsSync(newFilePath)
        if (fileSameNameExist) {
            ctx.throw(416, { name: 'FILE_ALREADY_EXIST', message: 'File with this name already exist' })//"range not satisfiable"
        }
    } catch (error) {
        if (error.message.indexOf('no such file or directory') !== -1) {
            ctx.throw(416, { name: 'FILE_NOT_FOUND', message: 'No such directory' })//"range not satisfiable"
        }
        ctx.throw(500, error)
    }

    if (fullPathToFile.indexOf(servCfg.storageDir) === -1) { // if user try to escape from storage dir
        ctx.throw(400, { name: 'INPUT_ERR', message: 'Incorrect path' })
    }
    if (newFilePath.indexOf(servCfg.storageDir) === -1) { // if user try to escape from storage dir
        ctx.throw(400, { name: 'INPUT_ERR', message: 'Incorrect path' })
    }

    fs.renameSync(fullPathToFile, newFilePath)
    //!!! call function without await coz we want to send answer to user and after make a confirm processing
    sendReplyToNS({ reqId: ctx.state.reqId, confirmId, paramUrl: '/storage/confirm' })

    log.info(`#${ctx.state.reqId}. ok`);
    ctx.body = {
        status: 'ok',
        response: {
            oldName: requestedPath,
            newName: newFilePath.split(servCfg.storageDir)[1] //delete our full path to file and make it relatively to user
        }
    }
};
export default renameHandler