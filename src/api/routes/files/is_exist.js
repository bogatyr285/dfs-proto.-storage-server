import path from 'path'
import fs from 'fs'

import { server as servCfg } from '../../../config'

import Logger from '../../../logger';
const log = Logger(__filename);

const isFileExistHandler = async (ctx, next) => {
    if (typeof ctx.request.fields==='undefined') {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Params not found" })
    }
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(ctx.request.fields, ctx.request.body)}`);
    const { path: requestedFile, size: requestedSize } = ctx.request.fields;

    if (!requestedFile) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Path unspecified" })
    }
    //TODO someday make size verification
    if (!requestedSize) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Size unspecified" })
    }
    let fullPathToFile = path.join(servCfg.storageDir, requestedFile)

    if (fullPathToFile.indexOf(servCfg.storageDir) === -1) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Path unspecified" })
    }
    let isFileExist = fs.existsSync(fullPathToFile)
    if (isFileExist) {
        ctx.throw(404, { name: 'FILE_ALREADY_EXIST', message: "File with this name already exist" })
    }
    log.info(`#${ctx.state.reqId}. ok`);
    return ctx.body = {
        status: 'ok',
        response: {
            path: requestedFile,
        }
    }
};
export default isFileExistHandler