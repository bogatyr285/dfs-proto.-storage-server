import path from 'path'
import fs, { mkdir } from 'fs'
import mkdirp from 'mkdirp'
import { server as servCfg } from '../../../config'
import { sendReplyToNS } from '../../../ns'
import Logger from '../../../logger';
const log = Logger(__filename);

const createFolderHandler = async (ctx, next) => {
    if (typeof ctx.request.fields === 'undefined') {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Params not found" })
    }
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(ctx.request.fields)}`);
    const { name: foldername, path: requestedPath, id: confirmId, isrepl: isRepl } = ctx.request.fields
    if (!foldername || !requestedPath) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "'path' or 'foldername' unspecified" })
    }
    if (!confirmId) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Request file confirmation id unspecified" })
    }
    let isAbsolutePath = path.isAbsolute(requestedPath)//check if provided path not begin from '/'

    if (!isAbsolutePath) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Incorrect path" })
    }
    let fullPath = path.join(servCfg.storageDir, requestedPath, foldername)

    if (fullPath.indexOf(servCfg.storageDir) === -1) { // if user try to escape from storage dir
        ctx.throw(400, { name: 'INPUT_ERR', message: 'Incorrect path' })
    }
    let pathExist = fs.existsSync(fullPath)
    if (pathExist) {
        ctx.throw(416, { name: 'DIR_EXIST', message: 'Directory already exist' })//"range not satisfiable"
    }

    mkdirp.sync(fullPath)

    //!!! call function without await coz we want to send answer to user and after make a confirm processing
    sendReplyToNS({ reqId: ctx.state.reqId, confirmId, paramUrl: '/storage/confirm', isRepl })//isRepl - if set up to true I confirm to NS that I create folder

    log.info(`#${ctx.state.reqId}. ok`);
    ctx.body = {
        status: 'ok',
        response: {
            created: fullPath.split(servCfg.storageDir)[1]
        }
    }
};
export default createFolderHandler