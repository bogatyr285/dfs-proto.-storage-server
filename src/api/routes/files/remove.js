import path from 'path'
import fs from 'fs'
import rimraf from 'rimraf'
import { server as servCfg } from '../../../config'
import { sendReplyToNS } from '../../../ns'
import Logger from '../../../logger';
const log = Logger(__filename);

const deleteHandler = async (ctx, next) => {
    if (typeof ctx.request.fields==='undefined') {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Params not found" })
    }
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(ctx.request.fields)}`);
    const { path: requestedPath } = ctx.request.fields;
    const confirmId = ctx.request.fields.id || '';
    if (!requestedPath) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Path unspecified" })
    }
    if (!confirmId) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Request file confirmation id unspecified" })
    }
    let isAbsolutePath = path.isAbsolute(requestedPath)//check if provided path not begin from '/'

    if (!isAbsolutePath) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Path unspecified" })
    }
    let fullPathToFile = path.join(servCfg.storageDir, requestedPath)

    try {
        let pathExist = fs.existsSync(fullPathToFile)
        if (!pathExist) {
            ctx.throw(416, { name: 'FILE_NOT_FOUND', message: 'No such directory' })//"range not satisfiable"
        }
    } catch (error) {
        if (error.message.indexOf('no such file or directory') !== -1) {
            ctx.throw(416, { name: 'FILE_NOT_FOUND', message: 'No such directory' })//"range not satisfiable"
        }
        ctx.throw(500, error)
    }

    if (fullPathToFile.indexOf(servCfg.storageDir) === -1) { // if user try to escape from storage dir
        ctx.throw(400, { name: 'INPUT_ERR', message: 'Incorrect path' })
    }
    //!!! call function without await coz we want to send answer to user and after make a confirm processing
    sendReplyToNS({ reqId: ctx.state.reqId, confirmId, paramUrl: '/storage/confirm' })
    rimraf.sync(fullPathToFile)

    log.info(`#${ctx.state.reqId}. ok`);
    ctx.body = {
        status: 'ok',
        response: {
            removed: requestedPath
        }
    }
};
export default deleteHandler