import path from 'path'
import fs from 'fs'
import mime from 'mime-types'
import { server as servCfg } from '../../../config'

import Logger from '../../../logger';
const log = Logger(__filename);

const downloadFileHandler = async (ctx, next) => {
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(ctx.query)}`);
    const { path: requestedFile } = ctx.query;
    if (!requestedFile) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Path unspecified" })
    }
    let isAbsolutePath = path.isAbsolute(requestedFile)//check if provided path not begin from '/'

    if (!isAbsolutePath) {
        ctx.throw(400, { name: 'INPUT_ERR', message: "Path unspecified" })
    }
    let fullPathToFile = path.join(servCfg.storageDir, requestedFile)

    let pathExist = fs.existsSync(fullPathToFile)
    if (!pathExist) {
        ctx.throw(404, { name: 'FILE_NOT_FOUND', message: "Requested file not found" })
    }
    let fileExist = fs.statSync(fullPathToFile).isFile()
    if (!fileExist) {
        ctx.throw(404, { name: 'FILE_NOT_FOUND', message: "Requested file not found" })
    }

    let filename = path.parse(requestedFile).base
    let fileMime = mime.lookup(requestedFile)
    let fileLastModif = fs.statSync(fullPathToFile).mtime
    
    log.info(`#${ctx.state.reqId}. ok`);
    ctx.attachment(filename)
    ctx.type = fileMime
    ctx.lastModified = fileLastModif
    ctx.body = fs.createReadStream(fullPathToFile)
};
export default downloadFileHandler