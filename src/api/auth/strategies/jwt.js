import {Strategy as JwtStrategy} from 'passport-jwt'
import {ExtractJwt} from 'passport-jwt'
import { jwt as jwtCfg } from '../../../config'

import Logger from '../../../logger'
const log = Logger(__filename);

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
    secretOrKey: jwtCfg.secret,
    issuer: jwtCfg.issuer,
};

//----------Passport JWT Strategy--------//

// Expect JWT in the http header

export default new JwtStrategy(jwtOptions, async function (payload, done) {
    log.debug(`Payload: ${JSON.stringify(payload)}`)

    return done(null, payload)
})