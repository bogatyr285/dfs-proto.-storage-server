import passport from 'koa-passport'
// const { User } = require('../../db/models')

import {  JwtStrategy } from'./strategies'
import Logger from '../../logger'
const log = Logger(__filename);

passport.use(JwtStrategy)
passport.serializeUser(function(user, done) {
    log.debug(`Serializing: ${JSON.stringify(user)}`);
    done(null, user);
})
passport.deserializeUser(async(id, done) => {
    log.debug(`Deserializing: ${id}`);
    // let user = await User.findById(id);
    // if (!user) {
    //     return done({ message: `User ID does not exist. Id:${id}` });
    // }
    return done(null, id/* user */)
})

export default passport;