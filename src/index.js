
import mkdirp from 'mkdirp'
import { server as servCfg, ns as nsCfg } from './config'
import api from './api'
import { chooseNSserver } from './ns'
import Logger from './logger';
const log = Logger(__filename);

process.on('uncaughtException', error => {
    log.error('Unhandled exeption:')
    log.error(`${error.name} ${error.message} ${error.stack}`);
});
process.on('unhandledRejection', error => {
    log.error('Unhandled rejection:')
    log.error(`${error.name} ${error.message} ${error.stack}`);
});


(async () => {
    try {
        mkdirp.sync(servCfg.storageDir)
        mkdirp.sync(servCfg.tmpUploadDir)
        mkdirp.sync(nsCfg.tokenSavePath)

        await chooseNSserver()
        await api();
    } catch (error) {
        console.log('Something went wrong:\n', error)
        process.exit(1)
    }
})();