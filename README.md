# DFS.storage server

> Distributed File System Storage Server. One of lab in university DS course. Provides file storage, synchonization, keep alive with NS server and file upload for users. 
### generate storage API documentation(will be avaible at ./docs/index.html)

```
$ npm run docs
```
#### Other part of this project:
* ** Client : **
	* https://bitbucket.org/bogatyr285/dfs-proto.-client/src/develop
* ** Name server: **
	* https://github.com/karust/dfs_ns

* ** To launch whole system: **
    * clone all repos(client, storage server, name server)
    * rename folders to `dfs_client`, `dfs_ns`, `dfs_ss` 
    * put `docker-compose.yml` in folder where `dfs_client`, `dfs_ns`, `dfs_ss` is located
    * run `docker-compose up --build`
    * now client available at address 10.0.0.30:3000

    ![Image of client interface](client.png)