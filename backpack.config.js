module.exports = {
    webpack: (config, options, webpack) => {
        // Perform customizations to config
        // Important: return the modified config

        // changes the name of the entry point from index -> main.js
        config.entry.main = [
            './src/index.js'
        ]
        //TODO This line enables escaping tranpiling for formidable because we can't load formidable with
        //import(we will get an error about "require('crypto') is not a function").
        // But with enabling this hot reload will be disabled and need every time recompile project 
           config.externals= [
               {
                   formidable: 'formidable',
               },
           ]
        config.plugins = [
            new webpack.DefinePlugin({ "global.GENTLY": false })
        ]
        node: {
            __dirname: true
        }
        return config
    },
}