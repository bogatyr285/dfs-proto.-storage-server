FROM mhart/alpine-node:10

RUN mkdir -p /projects/storage
WORKDIR /projects/storage

COPY package.json /projects/storage/package.json
COPY package-lock.json /projects/storage/package-lock.json

RUN cd /projects/storage \
    #&& apk --no-cache add --virtual builds-deps build-base python \
    && apk update && apk upgrade  \
    && apk add --no-cache bash git \
    && echo ">> Installing NPM packages" \
    && npm i 
COPY . /projects/storage

RUN echo ">> Build project" \
    && npm run build